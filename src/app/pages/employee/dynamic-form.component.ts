import { DynamicControlBase } from './dynamic-control-base';

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'dynamic-form',
    templateUrl: './dynamic-form.component.html'
})
export class DynamicFormComponent {
    @Input() controlItem: DynamicControlBase<any>;
    @Input() form: FormGroup;
};
