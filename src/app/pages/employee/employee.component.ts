import { StorageService } from '../../services/session.service';
import { DynamicControlBase } from './dynamic-control-base';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-employee',
    templateUrl: './employee.component.html'
})
export class EmployeeComponent implements OnInit {
    template: any;
    submittedData = [];
    empResults = [];
    keys: string[];
    controlList: any[];
    storeList: any[];
    isEdit: boolean;
    emplist: Array<any> = [];
    store: Array<any> = [];
    form: FormGroup = new FormGroup({});
    selectedItem: any;

    constructor(private storageService: StorageService) { }


    ngOnInit() {
        this.template = this.storageService.get('template');
        this.parseResult(this.template);
        this.store = JSON.parse(this.storageService.get('store')) || [];
    }

    parseResult(data: any) {
        this.controlList = JSON.parse(data);
        this.form = this.toFormGroup(this.controlList);
    }

    onUpdate(item) {

        this.controlList = [];
        const stores = JSON.parse(this.storageService.get('store'));
        const selectedItem = stores.filter(i => i.id == item.id);
        selectedItem.forEach(element => this.controlList.push(element.value));
        this.form = this.toFormGroup(this.controlList);
        this.isEdit = true;
        this.selectedItem = item;
    }

    updateSubmit() {
        const stores = JSON.parse(this.storageService.get('store'));
        this.delete(this.selectedItem);
        this.onSubmit();
        this.isEdit = false;
    }

    delete(obj) {
        this.store = JSON.parse(this.storageService.get('store'));
        this.store = this.store.filter(item => item.id !== obj.id);
        this.emplist = this.emplist.filter(item => item.id != obj.id);
        this.storageService.put('store', JSON.stringify(this.store));
    }

    onSubmit() {

        if (this.form.valid) {

            let controls = [];
            const uid = this.isEdit ? this.selectedItem.id : Math.round(Math.random() * 1000) + 1;
            this.controlList.forEach(element => {
                element.value = this.form.value[element.name];
                controls = Object.assign({}, element);
                this.store.push({
                    id: uid,
                    value: controls
                });
            });

            const obj = this.groupBy(this.store, 'id');
            this.emplist = [];
            var item = [];
            // tslint:disable-next-line:forin
            for (const key in obj) {
                if (obj.hasOwnProperty(key)) {
                    obj[key].forEach(element => {
                        item.push(
                            {
                                id: element.id,
                                [element.value.label]: [element.value.value].join()
                            }
                        );
                    });
                }

                const obj1 = {};
                $.each(item, function (i, o) {
                    $.extend(obj1, o);
                });

                this.emplist.push(obj1);
            }

            this.keys = Object.keys(this.emplist[0]);
            this.storageService.put('store', JSON.stringify(this.store));
            this.form.reset();
        }
    }


    groupBy(arr, key) {
        return arr.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    };

    toFormGroup(controlItems: DynamicControlBase<any>[]) {
        const group: any = {};
        controlItems.forEach(item => {
            group[item.name] = item.required ? new FormControl(item.value || '', Validators.required)
                : new FormControl(item.value || '');
        });
        return new FormGroup(group);
    }
}
