import { Component, OnInit, AfterViewInit } from '@angular/core';
import { StorageService } from '../../services/session.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements AfterViewInit {

  formBuilder: any;

  formData = [];
  options = {
    disabledActionButtons: ['data', 'save'],
    disableFields: ['autocomplete', 'button', 'date', 'checkbox-group', 'header', 'file', 'hidden', 'paragraph']
  };

  constructor(private storageService: StorageService) {

  }

  ngAfterViewInit() {
    const fbEditor = document.getElementById('fb-editor');
    this.formBuilder = $(fbEditor).formBuilder(this.options);
  }

  onSubmit() {
    const keyValue = JSON.parse(this.formBuilder.actions.getData('json'));
    this.formData = keyValue;
    this.storageService.put('template', JSON.stringify(this.formData));
    console.log(this.formData);
  }
}
